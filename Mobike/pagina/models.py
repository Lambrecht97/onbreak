from django.db import models

# Create your models here.
class usuario(models.Model):
    ADMINISTRADOR= 'A'
    FUNCIONARIO = 'F'
    CLIENTE = 'C'
    USUARIOS_CHOICES = [
        (ADMINISTRADOR, 'A'),
        (FUNCIONARIO, 'F'),
        (CLIENTE, 'C')
        
    ]
    id_usuario =models.IntegerField(primary_key=True)
    nombre_usuario=models.CharField(max_length=2000)
    a_paterno_u=models.CharField(max_length=2000)
    rut_u=models.CharField(max_length=12)
    email_u=models.EmailField()
    direccion_u=models.CharField(max_length=1000)
    telefono_u=models.CharField(max_length=10)
    feca_nac_u=models.DateField()
    tipo_usuario=models.CharField(
        max_length=1,
        choices=USUARIOS_CHOICES,
        default=ADMINISTRADOR
    )

class cliente(models.Model):
    id_cliente=models.IntegerField(primary_key=True)
    nombre_cli=model.CharField(max_length=2000)
    ap_cli=models.CharField(max_length=2000)
    dir_cli=models.CharField(max_length=2000)
    dir_trab_cli=models.CharField(max_length=2000)
    tel_cli=models.CharField(max_legth=20)
    rut_cli=models.CharField(max_length=12)
    num_cuenta_cli=models.IntegerField()
    email_cli=models.EmailField()
    comuna_cli=models.ForeignKey(comuna)

class administrador(models.Model):
    id_admin=models.IntegerField(primary_key=True,auto_created=True)
    nombre_admin=models.CharField(max_length=2000)
    ape_admin=models.CharField(max_length=2000)
    rut_admn=models.CharField(max_length=12)
    direccion_admin=models.CharField(max_length=2000)
    telefono_admin=models.CharField(max_length=20)
    email_admin=models.EmailField()

class funcionario(models.Model):
    id_fun=models.IntegerField(primary_key=True)
    nom_fun=models.CharField(max_length=2000)
    ape_fun=models.CharField(max_length=2000)
    rut_fun=models.CharField(max_length=12)
    direccion_fun=models.CharField(mnax_length=2000)
    telefono_fun=models.CharField(max_length=20)
    email_fun=models.EmailField()

class comuna(models.Model):
    id_comuna=models.IntegerField(primary_key=True)
    nom_comuna=models.CharField(max_length=2000)

class pago(models.Model):
    id_pago=models.IntegerField(primary_key=True)
    valoir_pago=models.IntegerField()

class banco(models.Model):
    id_banco=models.IntegerField(primary_key=True)
    nombre_banco=models.CharField(max_length=200)

class tarjeta(models.Model):
    id_tarjeta=models.IntegerField(primary_key=True)
    num_tarjeta=model.IntegerField()
    nombre_titular=models.CharField(max_length=200)
    apellido_titular=models.CharField(max_length=200)
    codigo_seguridad=models.CharField(('password'),max_length=3)
    fecha_expiracion=models.DateField()

class bicicleta(models.Model):
    DISPONIBLE= 'D'
    OCUPADA = 'O'
    BIBICLETA_CHOICES = [
        (DISPONIBLE, 'D'),
        (OCUPADA, 'O')
    ]
    id_bicicleta=models.IntegerField(primary_key=True)
    estado_bicicleta=models.CharField(
        max_length=1
        choices=BIBICLETA_CHOICES,
        default=DISPONIBLE
    )

class aparcamiento(models.Model):
    id_zona=models.IntegerField(primary_key=True)
    descripcion=models.CharField(max_length=10000)